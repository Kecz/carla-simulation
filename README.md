# CARLA Simulation

## Opis projektu
Celem niniejszego projektu była próba zrealizowania autonomicznego sterowania samochodem w środowisku zurbanizowanym. Głównym zadaniem auta było wyznaczenie trasy w sposób inteligentny i optymalny z punktu startowego do punktu końcowego. Podczas działania całego systemu agent bierze pod uwagę nie tylko przepisy poruszania się po drodze, ale również innym uczestników ruchu drogowego. Aby rozbudować projekt zdecydowano się na realizację środowiska wieloagentowego, tak by auta znajdujące się w nim były zdolne do rywalizacji. W tym celu przygotowano model, który jest zdolny do sterowania silnikiem i kierownicą w zależności od punktu do którego ma dotrzeć, reaguje na sygnalizatory drogowe w postaci świateł oraz jest odporny na kolizję. 

Szczegółowa dokumentacja projektu znajduje się w katalogu ``docs``.

Podczas realizacji projektu bazowano na środowisku symulacyjnym [CARLA](https://github.com/carla-simulator/carla) (Open Urban Driving Simulator) oraz wykorzystano dostarczone przez autorów środowiska Python API.

## Założenia projektowe
*	Środowisko wyścigowe przystosowane do wieloagentowego trybu gry.
*	Kontroler trajektorii pojazdu, sterowanie silnikiem oraz kierownicą.
*	Detekcja kluczowych obiektów (np. światła).
*	Algorytm zapobiegający kolizji w środowisku wieloagentowym.
*	Zestaw różnych algorytmów pokonywania określonej trasy.
*	Mechanizm rywalizacji algorytmów.

## Konfiguracja środowiska
Projekt został przygotowany w środowisku symulacyjnym CARLA w wersji 0.9.5. Jest to najnowsza wersja symulatora wspierająca system operacyjny Windows, przystosowana do pracy z Pythonem w wersji 3.7. 

#### Wymagania sprzętowe:
*	Quad-core Intel/AMD, 2,5 GHz lub szybszy
*	NVIDIA GeForce 470 GTX lub AMD Radeon 6870 lub nowsze
*	8 GB RAM
*	10 GB wolnej przestrzeni dyskowej na potrzeby symulatora
#### Wymagania dotyczące oprogramowania:
*	Unreal Engine 4.22.x
*	Interpreter Python 3.7 

W celu skonfigurowania środowiska należy przede wszystkim pobrać odpowiednią wersję symulatora z oficjalnego repozytorium. Następnie należy zainstalować odpowiednią wersję interpretera języka Python.
Po wykonaniu tych czynności należy sklonować repozytorium projektu do folderu docelowego. Przed przystąpieniem do kolejnego kroku zalecane jest utworzenie wirtualnego środowiska interpretera języka Python. Można tego dokonać poprzez rozszerzenie zmiennej systemowej PATH o ścieżkę interpretera oraz wykonanie następujących poleceń w konsoli systemu operacyjnego Windows:
*	```pip install virtualenv``` 
*   ```virtualenv <nazwa środowiska>```
*	```<folder źródłowy środowiska>\Scripts\activate.bat```

Gdy środowisko wirtualne zostanie aktywowane można przejść do instalacji niezbędnych bibliotek. W tym celu należy w folderze głównym projektu wykonać polecenie:
*	```pip install -r requirements.txt ```

Następnie należy zbudować kod źródłowy projektu ulokowany w folderze scr poprzez wykonanie polecenia:
*	```python setup.py install```
 lub
*	```pip install``` 

Gdy powyższe czynności zostaną wykonane pozostaje jedynie uruchomienie CARLI za pomocą pliku CarlaUE4.exe. Po załadowaniu symulacji możliwe jest uruchamianie plików startowych lub przygotowanych w ramach projektu testów. Przykładowa seria instrukcji pozwalająca uruchomić bazowy skrypt startowy przedstawiona została poniżej: 
*   ```cd start_scripts```
*	```python start_basic.py ```

W przypadku chęci dalszej realizacji projektu i korzystania z plików źródłowych CARLI konieczne może być zainstalowanie paczek źródłowych. Tyczy to się sytuacji, gdy otrzymujemy błąd, że nie możemy zimportować paczki „carla”.  W tym celu należy zlokalizować plik instalacyjny z oficjalnego repozytorium z rozszerzeniem „.egg” i posłużyć się modułem „easy_install”:
*	```python -m easy_install CARLA-0-9-5-python37.egg```
