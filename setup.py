# import os

from setuptools import setup


# def find_subdirectories(path):
#     return [f.path for f in os.scandir(path) if f.is_dir() and 'egg' not in f.path and 'pycache' not in f.path]
#
#
# def find_all_subdirectories(root):
#     subdirectories = find_subdirectories(root)
#     for directory in subdirectories:
#         deep_dir = find_subdirectories(directory)
#         subdirectories += deep_dir
#     for i, directory in enumerate(subdirectories):
#         subdirectories[i] = directory.replace('\\', '.')
#     return subdirectories.append(root)


setup(
    name="CARLA PG",
    version="1.0",
    author="CARLA TEAM",
    description="University project connected with CARLA simulator.",
    license="BSD",
    keywords="CARLA, autonomous cars, collision, traffic lights, simulator",
    packages=['src'],
    include_package_data=True,
    zip_safe=False,
)
