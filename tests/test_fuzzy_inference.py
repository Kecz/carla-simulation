# Copyright (c) 2017 Computer Vision Center (CVC) at the Universitat Autonoma de
# Barcelona (UAB).
#
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.

"""
Fuzzy inference for data provided by semantic segmentation camera.
"""

import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import matplotlib.pyplot as plt

try:
    import numpy
    from numpy.matlib import repmat
except ImportError:
    raise RuntimeError('cannot import numpy, make sure numpy package is installed')

PIXELS = 5000
DISTANCES = 3
TERRAIN_BOUNDARY = 5
POSITIONS = 3


def distance(object_height):
    """
    function for fuzzy inference about the distance to the object
    :param object_height: the height of the checked object
    :return: lexical value in terms of distance to the object
    """
    # input and output holding membership functions
    input1 = ctrl.Antecedent(np.arange(10, 300, 10), 'input1')
    output = ctrl.Consequent(np.arange(0, 26, 1), 'output')

    # auto-membership function
    linguistic_values = ['small', 'medium', 'big']
    input1.automf(names=linguistic_values)
    # custom membership functions
    linguistic_values_output = ['far', 'close', 'very_close']
    output.automf(names=linguistic_values_output)

    # input1['small'].view()
    # plt.show()

    # define the fuzzy relationship between input and output variables

    rule1 = ctrl.Rule(input1['small'], output['far'])
    rule2 = ctrl.Rule(input1['medium'], output['close'])
    rule3 = ctrl.Rule(input1['big'], output['very_close'])

    # rule1.view()
    # plt.show()

    # create a control system
    system = ctrl.ControlSystem([rule1, rule2, rule3])
    # simulate created control system
    system_simulation = ctrl.ControlSystemSimulation(system)

    # create a list to hold the distances to varied objects
    object_lexical_value = []

    # iterate through values of objects' heights
    # as there might be many objects to check distance to
    for measurement in object_height:
        #  pass inputs to the ControlSystem using Antecedent labels
        system_simulation.input['input1'] = measurement
        system_simulation.compute()
        object_numerical_value = system_simulation.output['output']
        # print(object_numerical_value)
        # once computed, append lexical value to list
        if object_numerical_value < 6:
            object_lexical_value.append('far')
        elif 6 <= object_numerical_value < 7.5:
            object_lexical_value.append('close')
        elif object_numerical_value >= 7.5:
            object_lexical_value.append('very_close')
        # print(object_lexical_value)

    # return list of distances
    return object_lexical_value


def position(object_center):
    """
    function for fuzzy inference about the position of the object
    :param object_center: the center of the checked object
    :return: numerical value of the position of the object
    """
    # input and output holding membership functions
    input1 = ctrl.Antecedent(np.arange(0, 600, 10), 'input1')
    output = ctrl.Consequent(np.arange(0, 26, 1), 'output')

    # auto-membership function
    linguistic_values = ['small', 'medium', 'big']
    input1.automf(names=linguistic_values)

    # custom membership function
    linguistic_values_output = ['left', 'center', 'right']
    output.automf(names=linguistic_values_output)

    # define the fuzzy relationship between input and output variables
    rules = (
        ctrl.Rule(input1['small'], output['left']),
        ctrl.Rule(input1['medium'], output['center']),
        ctrl.Rule(input1['big'], output['right'])
    )

    # create a control system
    system = ctrl.ControlSystem(rules)
    # simulate created control system
    system_simulation = ctrl.ControlSystemSimulation(system)

    #  pass inputs to the ControlSystem using Antecedent labels
    system_simulation.input['input1'] = object_center

    # crunch the numbers
    system_simulation.compute()
    # once computed, save the results
    object_numerical_value = system_simulation.output['output']
    # print(object_numerical_value)

    # output.view()
    # plt.show()

    return object_numerical_value
