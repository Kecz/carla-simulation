import carla
import cv2
import matplotlib.pyplot as plt
from carla import Transform, Location, Rotation
from loguru import logger as log
from matplotlib import animation

from src import consts
from src.tools.json_parser import ConfigParser
from src.traffic_light.brake_control_module.brake_control import BrakeControl

cam_width = 1600
cam_height = 900

sensor_tick = 0.1  # in seconds
plot_tick = sensor_tick*1000  # in miliseconds
video_fps = 1/sensor_tick   # frames per second

do_record = True
path_to_created_video = "test_video0.avi"

global brake_control, im, car_actor, ax, text, video_holder


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    transform = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    car_bp = blueprint_library.filter("vehicle.tesla.*")[0]
    global car_actor
    car_actor = world.spawn_actor(car_bp, transform)

    # Find the blueprint of the sensor.
    rgb_bp = blueprint_library.find('sensor.camera.rgb')
    # Modify the attributes of the blueprint to set image resolution and field of view.
    rgb_bp.set_attribute('image_size_x', str(cam_width))
    rgb_bp.set_attribute('image_size_y', str(cam_height))
    rgb_bp.set_attribute('fov', '110')
    # Set the time in seconds between sensor captures
    rgb_bp.set_attribute('sensor_tick', str(sensor_tick))
    # Provide the position of the sensor relative to the vehicle.
    transform = carla.Transform(carla.Location(x=0.8, z=1.7), Rotation(yaw=0, pitch=7))
    # Tell the world to spawn the sensor, don't forget to attach it to your vehicle actor.
    sensor = world.spawn_actor(rgb_bp, transform, attach_to=car_actor)

    global brake_control
    brake_control = BrakeControl(config_dict=config_parser.config, time_wait_after_red=2, rgb_cam_actor=sensor,
                                 calculating_method="fuzzy")

    log.info("Autopilot ON")
    car_actor.set_autopilot(True)

    output_img = brake_control.get_img_with_detection()
    output_img = cv2.cvtColor(output_img, cv2.COLOR_BGR2RGB)
    fig = plt.figure()
    global im, ax, text
    ax = fig.add_subplot(1, 1, 1)
    text = ax.text(50, 50, f'Brake value = {0}', fontsize=15, color='cyan')

    if do_record:
        global video_holder
        video_holder = cv2.VideoWriter(path_to_created_video, cv2.VideoWriter_fourcc(*'DIVX'), video_fps,
                                       (cam_width, cam_height))

    im = ax.imshow(output_img, animated=True)

    # this attribution is needed for correct animation, unless it seems unused
    ani = animation.FuncAnimation(fig, update_animation, interval=5)
    plt.show()

    video_holder.release()
    print("end")
    sensor.destroy()
    car_actor.destroy()
    log.info("Detection interrupted")


# this function argument is needed for correct animation, unless it seems unused
def update_animation(i):
    global brake_control, im, car_actor, ax, text, video_holder
    output_img = brake_control.get_img_with_detection()
    brake_value = brake_control.get_brake_value()

    if brake_value > 0:
        car_actor.set_autopilot(False)
        curr_control = car_actor.get_control()
        curr_control.brake = 1
        car_actor.apply_control(curr_control)
    else:
        car_actor.set_autopilot(True)

    text.set_text(f"brake value: {brake_value}")
    log.info(f"brake value: {brake_value}")

    if do_record:
        video_holder.write(output_img)

    output_img = cv2.cvtColor(output_img, cv2.COLOR_BGR2RGB)
    im.set_array(output_img)


if __name__ == "__main__":
    main()
