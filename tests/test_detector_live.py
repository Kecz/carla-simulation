import carla
import cv2
import matplotlib.pyplot as plt
from carla import Transform, Location, Rotation
from loguru import logger as log
from matplotlib import animation

from src import consts
from src.rgb_camera.image_data_server import ImageDataServer
from src.tools.json_parser import ConfigParser
from src.traffic_light.detector.detector import Detector


cam_width = 1600
cam_height = 900
sensor_tick = 0.05  # In seconds
plot_tick = sensor_tick*1000  # in miliseconds

global detector
global data_server
global im


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    transform = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]

    car_actor = world.spawn_actor(car_bp, transform)

    log.info("Autopilot ON")
    car_actor.set_autopilot(True)

    # Find the blueprint of the sensor.
    rgb_bp = blueprint_library.find('sensor.camera.rgb')
    # Modify the attributes of the blueprint to set image resolution and field of view.
    rgb_bp.set_attribute('image_size_x', str(cam_width))
    rgb_bp.set_attribute('image_size_y', str(cam_height))
    rgb_bp.set_attribute('fov', '110')
    # Set the time in seconds between sensor captures
    rgb_bp.set_attribute('sensor_tick', str(sensor_tick))
    # Provide the position of the sensor relative to the vehicle.
    transform = carla.Transform(carla.Location(x=0.8, z=1.7), Rotation(yaw=0, pitch=7))
    # Tell the world to spawn the sensor, don't forget to attach it to your vehicle actor.
    sensor = world.spawn_actor(rgb_bp, transform, attach_to=car_actor)

    global detector, data_server, im
    detector = Detector(config_dict=config_parser.config)
    data_server = ImageDataServer(sensor)

    img = data_server.get_bgr_image()
    output_img = detector.get_img_with_detection(img)
    output_img = cv2.cvtColor(output_img, cv2.COLOR_BGR2RGB)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    im = ax.imshow(output_img, animated=True)

    ani = animation.FuncAnimation(fig, update_image, interval=plot_tick)

    plt.show()

    sensor.destroy()
    car_actor.destroy()
    log.info("Detection interrupted")


def update_image(i):
    global detector, data_server, im
    img = data_server.get_bgr_image()
    output_img = detector.get_img_with_detection(img)
    output_img = cv2.cvtColor(output_img, cv2.COLOR_BGR2RGB)
    im.set_array(output_img)


if __name__ == "__main__":
    main()
