import math

import carla
import matplotlib.pyplot as plt

from src.base_routing.guide_class import Guide

waypoints_map_range = 5
finish_distance_approximation = 5

chosen_algorithm = "most_forward"
# chosen_algorithm = "a_star"
# chosen_algorithm = "heuristic"
chosen_algorithm = "shortest_euclidean_distance"


def main():
    print("Connecting")
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    world = client.get_world()
    map_ = world.get_map()
    all_waypoints = map_.generate_waypoints(waypoints_map_range)

    start_point = find_closest_waypoint_by_xy(all_waypoints, x=-50, y=-200)
    end_point = find_closest_waypoint_by_xy(all_waypoints, x=-130, y=130)

    print("Processing route")
    if chosen_algorithm == "most_forward":
        guide = Guide(start_point, end_point)
        route = getattr(guide, "guide_most_forward_route")(all_waypoints=all_waypoints,
                                                           waypoints_range=waypoints_map_range,
                                                           finish_approximation=finish_distance_approximation)
    elif chosen_algorithm == "a_star":
        guide = Guide(start_point, end_point)
        route = getattr(guide, "guide_a_star")(waypoints_range=waypoints_map_range, all_waypoints=all_waypoints)
    elif chosen_algorithm == "dijkstra":
        guide = Guide(start_point, end_point)
        route = getattr(guide, "guide_dijkstra")(waypoints_range=waypoints_map_range, all_waypoints=all_waypoints)
    elif chosen_algorithm == "shortest_euclidean_distance":
        guide = Guide(start_point, end_point)
        route = getattr(guide, "guide_shortest_euclidean_distance")(waypoints_range=waypoints_map_range,
                                                                    all_waypoints=all_waypoints)

    total_distance = 0
    for i in range(len(route)-1):
        total_distance += heuristic_distance(route[i+1], route[i])
    print(f"Total distance {total_distance}")
    print("Plotting")
    for way_point in all_waypoints:
        plt.scatter(way_point.transform.location.x, way_point.transform.location.y, s=0.2, c="b")
    for wp in route:
        plt.scatter(wp.transform.location.x, wp.transform.location.y, s=[10], c="g")
    plt.scatter(start_point.transform.location.x, start_point.transform.location.y, s=[30], c="r")
    plt.scatter(end_point.transform.location.x, end_point.transform.location.y, s=[30], c="k")
    plt.show()


def find_closest_waypoint_by_xy(list_of_waypoints, x, y):
    best_dist = math.inf
    best_waypoint = list_of_waypoints[0]
    for waypoint in list_of_waypoints:
        x_distance = waypoint.transform.location.x - x
        y_distance = waypoint.transform.location.y - y
        dist = math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))
        if dist < best_dist:
            best_dist = dist
            best_waypoint = waypoint
    return best_waypoint


def heuristic_distance(waypoint1, waypoint2):
    # Distance between two waypoints
    x_distance = waypoint1.transform.location.x - waypoint2.transform.location.x
    y_distance = waypoint1.transform.location.y - waypoint2.transform.location.y
    return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))


if __name__ == "__main__":
    main()
