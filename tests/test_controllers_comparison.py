"""
Env:
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
"""

import carla

from src import consts
from src.colision_detection.collision_avoidance import CollisionDetector
from src.navigation.automatic_control import navigation_loop
from src.tools.json_parser import ConfigParser
from src.traffic_light.brake_control_module.brake_control import BrakeControl

world_actors_list = []

cam_width = 1600
cam_height = 900
sensor_tick = 1


def main():
    # initialization
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    world = client.get_world()
    map_ = world.get_map()
    blueprint_library = world.get_blueprint_library()
    spawn_points_list = map_.get_spawn_points()
    spawn = spawn_points_list[0]

    # Spawn on a sidewalk near a plant
    spawn.location.x -= 5.8

    destination = spawn_points_list[10]

    # creating vehicle
    car_bp = blueprint_library.filter("model3")[0]
    vehicle = world.spawn_actor(car_bp, spawn)
    world_actors_list.append(vehicle)

    # connecting collision detection system
    anti_collision_sys = CollisionDetector(world, vehicle)
    world_actors_list.append(anti_collision_sys)

    # Creating RGB Camera
    rgb_camera_bp = blueprint_library.find('sensor.camera.rgb')
    rgb_camera_bp.set_attribute('image_size_x', str(cam_width))
    rgb_camera_bp.set_attribute('image_size_y', str(cam_height))
    rgb_camera_bp.set_attribute('fov', '110')
    rgb_camera_bp.set_attribute('sensor_tick', str(sensor_tick))
    cam_transform = carla.Transform(carla.Location(x=0.8, z=1.7), carla.Rotation(yaw=0, pitch=7))
    rgb_camera = world.spawn_actor(rgb_camera_bp, cam_transform, attach_to=vehicle)
    world_actors_list.append(rgb_camera)

    # Creating traffic light detection system
    traffic_light_detection_system = BrakeControl(config_dict=config_parser.config, time_wait_after_red=2,
                                                  rgb_cam_actor=rgb_camera, calculating_method="fuzzy")

    # running control (change controller type in config.json)
    navigation_loop(client=client, config_dict=config_parser.config, actor=vehicle, destination=destination,
                    anti_collision_sys=anti_collision_sys, logs=False,
                    traffic_light_detection_system=traffic_light_detection_system)

    for act in world_actors_list:
        act.destroy()


if __name__ == '__main__':
    try:
        main()

    finally:
        for actor in world_actors_list:
            actor.destroy()
