from collections import deque
import math

import numpy as np

import carla
from pid.pid import get_speed


class VehiclePIDController():
    """
    PID kontroler to połączenie dwóch sterowników PID (bocznych i podłużnych) do wykonywania kontroli niskiego poziomu pojazdu od strony klienta
    """

    def __init__(self, vehicle, args_lateral=None, args_longitudinal=None):
        """

         : param args_lateral: słownik argumentów do ustawienia bocznego kontrolera PID za pomocą następującej semantyki:
                             K_P - proporcjonalny
                             K_D -- rozniczkowy
                             K_I -- calkowy term
        : param args_longitudinal: słownik argumentów do ustawienia podłużnego regulatora PID
        """
        if not args_lateral:
            args_lateral = {'K_P': 1.0, 'K_D': 0.0, 'K_I': 0.0}
        if not args_longitudinal:
            args_longitudinal = {'K_P': 1.0, 'K_D': 0.0, 'K_I': 0.0}

        self._vehicle = vehicle
        self._world = self._vehicle.get_world()
        self._lon_controller = PIDLongitudinalController(self._vehicle, **args_longitudinal)
        self._lat_controller = PIDLateralController(self._vehicle, **args_lateral)

    def run_step(self, target_speed, waypoint):
        """
        Wykonaj jeden krok kontroli przy użyciu zarówno bocznych, jak i podłużnych sterowników PID, aby osiągnąć docelowy punkt trasy
         przy danej prędkości docelowej.
         : param target_speed: pożądana prędkość pojazdu
         : param waypoint: lokalizacja docelowa zakodowana jako waypoint
         : return: odległość (w metrach) do punktu trasy
        """
        throttle = self._lon_controller.run_step(target_speed)
        steering = self._lat_controller.run_step(waypoint)

        control = carla.VehicleControl()
        control.steer = steering
        control.throttle = throttle
        control.brake = 0.0
        control.hand_brake = False
        control.manual_gear_shift = False

        return control


class PIDLongitudinalController():


    def __init__(self, vehicle, K_P=1.0, K_D=0.0, K_I=0.0, dt=0.03):

        self._vehicle = vehicle
        self._K_P = K_P
        self._K_D = K_D
        self._K_I = K_I
        self._dt = dt
        self._e_buffer = deque(maxlen=30)

    def run_step(self, target_speed, debug=False):

        current_speed = get_speed(self._vehicle)

        if debug:
            print('Current speed = {}'.format(current_speed))

        return self._pid_control(target_speed, current_speed)

    def _pid_control(self, target_speed, current_speed):
        """
       Oszacuj przepustnicę pojazdu na podstawie równań PID
         : param target_speed: prędkość docelowa w km / h
         : param current_speed: aktualna prędkość pojazdu w km / h
         : return: sterowanie przepustnicą w zakresie [0, 1]
        """
        _e = (target_speed - current_speed)
        self._e_buffer.append(_e)

        if len(self._e_buffer) >= 2:
            _de = (self._e_buffer[-1] - self._e_buffer[-2]) / self._dt
            _ie = sum(self._e_buffer) * self._dt
        else:
            _de = 0.0
            _ie = 0.0

        return np.clip((self._K_P * _e) + (self._K_D * _de / self._dt) + (self._K_I * _ie * self._dt), 0.0, 1.0)


class PIDLateralController():


    def __init__(self, vehicle, K_P=1.0, K_D=0.0, K_I=0.0, dt=0.03):

        self._vehicle = vehicle
        self._K_P = K_P
        self._K_D = K_D
        self._K_I = K_I
        self._dt = dt
        self._e_buffer = deque(maxlen=10)

    def run_step(self, waypoint):
        """
        Wykonaj jeden krok kontroli bocznej, aby skierować pojazd w kierunku określonej trasy.
         : param waypoint: docelowy waypoint
         : return: sterowanie kierownicą w zakresie [-1, 1] gdzie:
             -1 oznacza maksymalne sterowanie w lewo
             +1 maksymalne sterowanie w prawo
        """
        return self._pid_control(waypoint, self._vehicle.get_transform())

    def _pid_control(self, waypoint, vehicle_transform):
        """
        Oszacuj kąt skrętu pojazdu na podstawie równań PID
         : param waypoint: docelowy waypoint
         : param Vehicle_transform: prąd transformacji pojazdu
         : return: sterowanie kierownicą w zakresie [-1, 1]
        """
        v_begin = vehicle_transform.location
        v_end = v_begin + carla.Location(x=math.cos(math.radians(vehicle_transform.rotation.yaw)),
                                         y=math.sin(math.radians(vehicle_transform.rotation.yaw)))

        v_vec = np.array([v_end.x - v_begin.x, v_end.y - v_begin.y, 0.0])
        w_vec = np.array([waypoint.transform.location.x -
                          v_begin.x, waypoint.transform.location.y -
                          v_begin.y, 0.0])
        _dot = math.acos(np.clip(np.dot(w_vec, v_vec) /
                                 (np.linalg.norm(w_vec) * np.linalg.norm(v_vec)), -1.0, 1.0))

        _cross = np.cross(v_vec, w_vec)
        if _cross[2] < 0:
            _dot *= -1.0

        self._e_buffer.append(_dot)
        if len(self._e_buffer) >= 2:
            _de = (self._e_buffer[-1] - self._e_buffer[-2]) / self._dt
            _ie = sum(self._e_buffer) * self._dt
        else:
            _de = 0.0
            _ie = 0.0

        return np.clip((self._K_P * _dot) + (self._K_D * _de /
                                             self._dt) + (self._K_I * _ie * self._dt), -1.0, 1.0)