import carla
import time
from carla import Transform, Location, Rotation

from src.rgb_camera.image_data_server import ImageDataServer

path_to_save = "images/"


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    transform = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]

    car_actor = world.spawn_actor(car_bp, transform)

    print("Autopilot ON")
    car_actor.set_autopilot(True)

    # Find the blueprint of the sensor.
    rgb_bp = blueprint_library.find('sensor.camera.rgb')
    # Modify the attributes of the blueprint to set image resolution and field of view.
    rgb_bp.set_attribute('image_size_x', '1920')
    rgb_bp.set_attribute('image_size_y', '1080')
    rgb_bp.set_attribute('fov', '110')
    # Set the time in seconds between sensor captures
    rgb_bp.set_attribute('sensor_tick', '0.9')
    # Provide the position of the sensor relative to the vehicle.
    transform = carla.Transform(carla.Location(x=0.8, z=1.7))
    # Tell the world to spawn the sensor, don't forget to attach it to your vehicle actor.
    sensor = world.spawn_actor(rgb_bp, transform, attach_to=car_actor)

    # MAX 1 listen for sensor, otherwise error
    # On the first 2 options debuger is not working for some reason

    # First option: function is receiving only 1 argument - data from camera
    # sensor.listen(receiver)

    # Second option: function is receiving any number of arguments
    # sensor.listen(lambda data: receiver2(data, "abc"))

    # Third option: curr image is stored inside class and can be accessed at any time
    data_server = ImageDataServer(sensor)   # Connecting RGB Cam with data server

    # Saving image, additional argument "path" is needed so lambda has to be used
    # sensor.listen(lambda data: data_server.save_image(data, path_to_dir=path_to_save))

    # Calling more than 1 functions on listen, 'or' is needed because 'and' if None is returned from first condition,
    # don't execute next conditions
    # sensor.listen(lambda data: data_server.update_curr_image(data) or data_server.save_image(data, path_to_dir=path_to_save))

    img = data_server.get_rgb_image()
    data_server.plot_image(img)
    time.sleep(1)

    img = data_server.get_rgba_image()
    data_server.plot_image(img)
    time.sleep(1)

    img = data_server.get_bgra_image()
    data_server.plot_image(img)
    time.sleep(1)

    img = data_server.get_bgr_image()
    data_server.plot_image(img)
    time.sleep(1)

    # img = data_server.get_PIL_RGB_image()
    # data_server.plot_image(img)
    # time.sleep(1)

    time.sleep(5)
    sensor.destroy()
    car_actor.destroy()

    print("End")


def receiver(img_data):
    print(img_data)


def receiver2(img_data, name):
    print(name)
    print(img_data)


if __name__ == "__main__":
    main()
