import carla
import time

from src.util.parse_arguments import parse_arguments
from src.traffic_light.create_light import create_race_start_light


def spawn_lights():
    args = parse_arguments()
    change_light_after = 8

    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    # initialization
    world = client.get_world()
    map_ = world.get_map()
    spawn_points_list = map_.get_spawn_points()
    spawn1 = spawn_points_list[args.lights_start_one]
    spawn2 = spawn_points_list[args.lights_start_two]
    create_race_start_light(spawn1, world=world, change_after=change_light_after)
    create_race_start_light(spawn2, world=world, change_after=change_light_after)
    time.sleep(3*change_light_after)


if __name__ == "__main__":
    spawn_lights()
