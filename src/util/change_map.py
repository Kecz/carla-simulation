import sys
import carla

maps = ["Town01", "Town02", "Town03", "Town04", "Town05", "Town06", "Town07"]


def main():
    print("Changing map to: " + str(maps[int(sys.argv[1])]))
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds
    client.load_world(maps[int(sys.argv[1])])
    client.reload_world()


if __name__ == "__main__":
    main()



