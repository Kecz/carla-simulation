import argparse
import glob
from PIL import Image
from PIL import ImageOps


def main():
    """
    Script to augment dataset by applying horizontal flip to images.
    :return: None
    """
    parser = parse_args()

    for img_path in glob.glob(f"{parser.img_dir}/*.png"):
        if "aug" not in img_path:
            # Flip image
            img = Image.open(img_path)
            img = ImageOps.mirror(img)
            splitted_path = img_path.split(".")
            new_path = f"{splitted_path[0]}_aug.{splitted_path[1]}"
            print(f"Created new augmented image: {new_path}")
            img.save(new_path)

            # Create new labels and save them
            labels_path = f"{splitted_path[0]}.txt"
            new_label_path = f"{splitted_path[0]}_aug.txt"
            with open(labels_path, "r") as old_labels, open(new_label_path, "w") as new_labels:
                for line in old_labels.readlines():
                    annotations = line.split(" ")
                    new_labels.write(" ".join([annotations[0], str(1 - float(annotations[1])), annotations[2],
                                               annotations[3], annotations[4]]))


def parse_args():

    parser = argparse.ArgumentParser()

    parser.add_argument('--img_dir', '-d', help='path directory with images to be augmented', type=str)

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
