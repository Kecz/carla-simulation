def get_sensor_data():
    return {
        'front middle': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '6'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'front right': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '6'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 15,
                'pitch': 0,
                'roll': 0
            }
        },
        'front left': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '6'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -15,
                'pitch': 0,
                'roll': 0
            }
        },
        'front right close': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.5',
                'distance': '4'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 20,
                'pitch': 0,
                'roll': 0
            }
        },
        'front left close': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.5',
                'distance': '4'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -20,
                'pitch': 0,
                'roll': 0
            }
        },
        'front mid close': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.9',
                'distance': '4'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'front left 1 high speed': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '10'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -12,
                'pitch': 0,
                'roll': 0
            }
        },
        'front left 2 high speed': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '11'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -5,
                'pitch': 0,
                'roll': 0
            }
        },
        'front right 1 high speed': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '10'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 12,
                'pitch': 0,
                'roll': 0
            }
        },
        'front right 2 high speed': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '11'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 5,
                'pitch': 0,
                'roll': 0
            }
        },
        'front middle high speed': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.8',
                'distance': '12'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'high speed crash': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '15'
            },
            'location': {
                'z': 1.4,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'super high speed crash': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '25'
            },
            'location': {
                'z': 1.4,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 1': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '2.5'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 45,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 2': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '2.0'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 67.5,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 3': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '1.5'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 90,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 1': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '2.5'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -45,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 2': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '2.0'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -67.5,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 3': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '0.7',
                'distance': '1.5'
            },
            'location': {
                'z': 1.6,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -90,
                'pitch': 0,
                'roll': 0
            }
        }
    }


