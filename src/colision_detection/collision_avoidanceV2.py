import glob
import os
import sys
import random
import time
import numpy as np
import math
import copy
from src.colision_detection.sensor_dataV2 import get_sensor_data

from carla import Transform, Rotation, Location, VehicleControl, Timestamp

class CollisionDetector:

    def __init__(self, world, vehicle):
        self.world = world
        self.vehicle = vehicle

        self.blueprint_library = self.world.get_blueprint_library()
        self.sensors_data = get_sensor_data()
        self.control = VehicleControl()        

        self.collision_hist = []
        self.sensors_list = {}
        self.actor_list = []

        self.isDestroyed = False
        self.is_collision_detected = True

        self.row_1 = [False, False]
        self.row_2 = [False, False, False]
        self.row_3 = [False, False, False]
        self.right_site = False
        self.left_site = False

        self.row_1_frame = [0, 0]
        self.row_2_frame = [0, 0, 0]
        self.row_3_frame = [0, 0, 0]

        self.throttle_amt = 0.7
        self.light_turn = 0.3
        self.hard_turn = 0.6
        self.light_break = 0.1
        self.hard_break = 0.5

        self.frames_margin = 5

        self.kmh = 0

    def destroy(self):
        self.isDestroyed = True
        for actor in self.actor_list:
            actor.destroy()
        print("Cleaned")

    def run(self):        
        self.prepare_obstacle_sensors()
        self.add_collision_sensor()
        self.add_obstacle_sensors()
        self.world.on_tick(lambda data: self.control_Loop(data))

    def prepare_obstacle_sensors(self):
        for sensor_name in self.sensors_data:
            sensor = self.sensors_data[sensor_name]
            blueprint = self.blueprint_library.find("sensor.other.obstacle")
            blueprint = self.sensor_set_many_attributes(blueprint, sensor['bp_attributes'])
            transform = self.get_transform(sensor['location'], sensor['rotation'])
            self.sensors_list[sensor_name] = self.world.spawn_actor(blueprint, transform, attach_to=self.vehicle)
            self.actor_list.append(self.sensors_list[sensor_name])

    def add_collision_sensor(self):
        self.sensors_list['collision'] = self.world.spawn_actor(
            self.blueprint_library.find("sensor.other.collision"),
            Transform(Location(z=0.7, x=2.5)), attach_to=self.vehicle)
        self.actor_list.append(self.sensors_list['collision'])
        self.sensors_list['collision'].listen(lambda event: self.handle_collision(event))

    def add_obstacle_sensors(self):
        self.sensors_list['row 1 right'].listen(lambda event: self.col_event_r1_right(event))
        self.sensors_list['row 1 left'].listen(lambda event: self.col_event_r1_left(event))

        self.sensors_list['row 2 right'].listen(lambda event: self.col_event_r2_right(event))
        self.sensors_list['row 2 left'].listen(lambda event: self.col_event_r2_left(event))
        self.sensors_list['row 2 middle'].listen(lambda event: self.col_event_r2_left(event))

        self.sensors_list['row 3 right'].listen(lambda event: self.col_event_r3_right(event))
        self.sensors_list['row 3 left'].listen(lambda event: self.col_event_r3_left(event))
        self.sensors_list['row 3 middle'].listen(lambda event: self.col_event_r3_left(event))
        
        self.sensors_list['right_side 1'].listen(lambda _: self.col_event_right_site())
        self.sensors_list['right_side 2'].listen(lambda _: self.col_event_right_site())
        self.sensors_list['right_side 3'].listen(lambda _: self.col_event_right_site())

        self.sensors_list['left_side 1'].listen(lambda _: self.col_event_left_site())
        self.sensors_list['left_side 2'].listen(lambda _: self.col_event_left_site())
        self.sensors_list['left_side 3'].listen(lambda _: self.col_event_left_site())

    def sensor_set_many_attributes(self, bp, attributes):
        for attribute in attributes:
            bp.set_attribute(attribute, attributes[attribute])
        return bp

    def get_transform(self, loc, rot):
        location = Location(x=loc['x'], y=loc['y'], z=loc['z'])
        rotation = Rotation(pitch=rot['pitch'], yaw=rot['yaw'], roll=rot['roll'])
        transform = Transform(location, rotation)
        return transform

    def handle_collision(self, event):
        a=2
        # print("COLLISION")
        #for actor in env.actor_list:
        #    actor.destroy()
        #self.vehicle = self.world.spawn_actor(self.model3, self.transform)
        #self.run()

    def get_obstacle_with_sensor_attribute(self, attribute_list):
        blueprint = self.blueprint_library.find("sensor.other.collision")
        for attribute in attribute_list:
            blueprint.set_attribute(attribute)
        return blueprint

    def get_data_from_event(self, event):
        str_data = str(event) \
            .replace('ObstacleDetectionEvent(frame=', '') \
            .replace(', timestamp=', ' ') \
            .replace(', other_actor=', ' ')
        arr_data = str_data.split(' ')

        frame = int(arr_data[0])
        timestamp = arr_data[1]
        other_actor = arr_data[2]
        return frame

    # ========== row 1 =========
    def col_event_r1_right(self, event):
        self.row_1[0] = True
        self.right_site = True
        self.row_1_frame[0] = self.get_data_from_event(event)

    def col_event_r1_left(self,event):
        self.row_1[1] = True
        self.left_site = True
        self.row_1_frame[1] = self.get_data_from_event(event)

    # ========== row 2 ==========
    def col_event_r2_right(self, event):
        self.row_2[0] = True
        self.right_site = True
        self.row_2_frame[0] = self.get_data_from_event(event)

    def col_event_r2_middle(self, event):
        self.row_2[1] = True
        self.row_2_frame[1] = self.get_data_from_event(event)

    def col_event_r2_left(self, event):
        self.row_2[2] = True
        self.left_site = True
        self.row_2_frame[2] = self.get_data_from_event(event)

    # ========== row 3 ==========
    def col_event_r3_right(self, event):
        self.row_3[0] = True
        # self.right_site = True
        self.row_3_frame[0] = self.get_data_from_event(event)

    def col_event_r3_middle(self, event):
        self.row_3[1] = True
        self.row_3_frame[1] = self.get_data_from_event(event)

    def col_event_r3_left(self, event):
        self.row_3[2] = True
        # self.left_site = True
        self.row_3_frame[2] = self.get_data_from_event(event)
    #============================

    def col_event_right_site(self):
        self.right_site = True

    def col_event_left_site(self):
        self.left_site = True

    def control_Loop(self, _):
        if not self.isDestroyed:
            self.set_velocity()
            # print(self.kmh)
            word_frame = self.world.wait_for_tick().frame_count
            self.clear_sensor_data(word_frame)
            self.check_detectors_state()
            self.set_colision_state()
            # print(self.is_collision_detected)

    def set_colision_state(self):
        state = False
        for elem in self.row_1:
            if elem:
                state = True
                break

        if not state:
            for elem in self.row_2:
                if elem:
                    state = True
                    break
        
        if not state:
            for elem in self.row_3:
                if elem:
                    state = True
                    break
        self.is_collision_detected = state

    def set_velocity(self):    
        V = self.vehicle.get_velocity()
        self.kmh = int(3.6 * math.sqrt(V.x ** 2 + V.y ** 2 + V.z ** 2))

    def check_detectors_state(self):
        if self.row_1[0] or self.row_1[0]:
            self.set_control(brake=1)
        elif self.row_2[0]:
            if not self.left_site:
                self.set_control(steer=-self.hard_turn)
            else:
                self.set_control(brake=self.hard_break)
        elif self.row_2[2]:
            if not self.right_site:
                self.set_control(steer=self.hard_turn)
            else:
                self.set_control(brake=self.hard_break)
        elif self.row_2[1]:
            if not self.left_site:
                self.set_control(steer=self.hard_turn)
            elif not self.right_site:
                self.set_control(steer=self.hard_turn)
            else:
                self.set_control(brake=self.hard_break)
        elif self.row_3[0]:
            if not self.left_site:
                self.set_control(steer=-self.light_turn)
            else:
                self.set_control(brake=self.light_break)
        elif self.row_3[2]:
            if not self.right_site:
                self.set_control(steer=self.light_turn)
            else:
                self.set_control(brake=self.light_break)
        elif self.row_3[1]:
            if not self.left_site:
                self.set_control(steer=-self.light_turn)
            elif not self.right_site:
                self.set_control(steer=self.light_turn)
            else:
                self.set_control(brake=self.light_break)

    def clear_sensor_data(self, word_frame):
        for i in range(len(self.row_1_frame)):
            if (self.row_1_frame[i] + self.frames_margin) < word_frame:
                self.row_1[i] = False

        for i in range(len(self.row_2_frame)):
            if (self.row_2_frame[i] + self.frames_margin) < word_frame:
                self.row_2[i] = False

        for i in range(len(self.row_3_frame)):
            if (self.row_3_frame[i] + self.frames_margin) < word_frame:
                self.row_3[i] = False   
        self.right_site = False
        self.left_site = False

    def set_control(self, throttle=0.0, brake=0.0, steer=0.0):
        self.is_collision_detected = True
        self.control.throttle = throttle
        self.control.brake = 1
        self.control.steer = steer

    def get_control(self):
        return copy.copy(self.control)


