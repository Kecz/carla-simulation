import json
import sys

from loguru import logger
from typing import List


class ConfigParser:
    def __init__(self, config_json_dir: str):
        self._config_json_dir = config_json_dir
        self.config = self.read_config()

    def read_config(self) -> dict:
        try:
            with open(self._config_json_dir, "r") as config_file:
                json_dict = json.load(config_file)
            return json_dict
        except FileNotFoundError as e:
            logger.error(e)
            sys.exit(1)

    def change_value(self, key_path: List[str], new_value):
        changing_command = 'self.config'
        for key in key_path:
            changing_command = f"{changing_command}['{key}']"
        exec(f'{changing_command} = {new_value}')
